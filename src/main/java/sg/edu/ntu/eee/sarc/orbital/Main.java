package sg.edu.ntu.eee.sarc.orbital;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Locale;

/**
 * The Main view controller, which also acts as the starting point of this Swing program
 */
class Main {

    //region UI variables
    private JPanel panelMain;
    private JTabbedPane tabbedPane1;
    private JList parameterList;
    private JButton applyDateTimeButton;
    private DateTimePicker startDateTimePicker;
    private DateTimePicker endDateTimePicker;
    private JTextField minimumField;
    private JButton applyMaxMinButton;
    private JLabel dataTypeLabel;
    private JLabel unitLabel;
    private JTextField maximumField;

    private JMenuItem openItem;
    private JMenuItem closeItem;
    //endregion

    private DatabaseHelper databaseHelper = new DatabaseHelper();

    //region Main function, entry point of the entire application
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(Main::createAndShowGUI);
    }
    //endregion

    //region Initializer functions

    private ArrayList<ConsolePane> tabs = new ArrayList<>();

    private Main() {
        addTab(null, null);

        parameterList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        parameterList.setLayoutOrientation(JList.VERTICAL);
        parameterList.setVisibleRowCount(-1);
        parameterList.addListSelectionListener(this::listValueChanged);

        tabbedPane1.addChangeListener(this::tabChanged);

        applyDateTimeButton.addActionListener(this::applyDate);
        applyMaxMinButton.addActionListener(this::applyMaxMin);
    }

    /**
     * Creates and displays the GUI
     */
    private static void createAndShowGUI() {
        JFrame frame = new JFrame("O.R.B.I.T.A.L. Console");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Main main = new Main();
        frame.setJMenuBar(main.createMenuBar());
        frame.setContentPane(main.panelMain);

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Creates the menu bar and assigns every menu item its function
     * @return A `JMenuBar` object
     */
    private JMenuBar createMenuBar() {
        JMenuBar menu = new JMenuBar();

        // Top level menu
        JMenu fileMenu = new JMenu("File");
        JMenu toolsMenu = new JMenu("Tools");
        JMenu helpMenu = new JMenu("Help");

        // File sub-items
        openItem = new JMenuItem("Open");
        openItem.addActionListener(event -> openAction());
        fileMenu.add(openItem);
        closeItem = new JMenuItem("Close");
        closeItem.addActionListener(event -> closeDatabase());
        closeItem.setEnabled(false);
        fileMenu.add(closeItem);
        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.addActionListener(event -> System.exit(0));
        fileMenu.add(exitItem);

        // Tools sub-items
        JMenuItem wizardItem = new JMenuItem("Data Assistant");
        wizardItem.addActionListener((ActionEvent event) -> {
            JFrame dataFrame = new JFrame("Data Assistant");
            dataFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            dataFrame.setContentPane(new DataAssistant().dataAssistantPanel);
            dataFrame.pack();
            dataFrame.setVisible(true);
        });
        toolsMenu.add(wizardItem);

        // Help sub-items
        JMenuItem aboutItem = new JMenuItem("About");
        aboutItem.addActionListener((ActionEvent event) ->
                JOptionPane.showMessageDialog(panelMain, "O.R.B.I.T.A.L.: Orbital Relational Basic Integrated Tasking Analysis and Listing\nVersion 0.1\n\nDeveloped by Pan Ziyue\nSaRC Intern 2018"));
        helpMenu.add(aboutItem);

        // Add the top level menu into the Menu Bar object
        menu.add(fileMenu);
        menu.add(toolsMenu);
        menu.add(helpMenu);

        return menu;
    }

    private void openAction() {
        // Show File Open dialog
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Database Files", "db");
        fileChooser.setFileFilter(filter);

        int showOpenDialog = fileChooser.showOpenDialog(null);
        if (showOpenDialog != JFileChooser.APPROVE_OPTION) {
            // User did not choose anything
            return;
        }

        File selectedFile = fileChooser.getSelectedFile();
        String extension = "";
        int i = selectedFile.getName().lastIndexOf('.');
        if (i > 0) {
            extension = selectedFile.getName().substring(i+1);
        }

        // Check for file validity
        if (!extension.equals("db")) {
            // You might want to do additional invalid file checking beyond just the extension
            JOptionPane.showMessageDialog(null, "You have selected an invalid file type", "Invalid file", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // File should be valid beyond this point, open the database
        openDatabase(selectedFile.getAbsolutePath());
    }

    //endregion

    //region Database related functions

    /**
     * Opens a database file
     *
     * @param filename File name to open
     */
    private void openDatabase(String filename) {
        // Open the database file first
        try {
            databaseHelper.openDatabase(filename);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(panelMain, "Error opening database file\nError message: " + e.getMessage(), "Error opening database file!", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return;
        }

        // Enable all date controls
        startDateTimePicker.setEnabled(true);
        endDateTimePicker.setEnabled(true);
        applyDateTimeButton.setEnabled(true);

        // Disable all min/max controls until user selects a particular parameter
        minimumField.setEnabled(false);
        maximumField.setEnabled(false);
        applyMaxMinButton.setEnabled(false);

        // Get all columns from metadata table and populate side list
        ArrayList<String> columns = new ArrayList<>();
        try {
            databaseHelper.getColumns(columns);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(panelMain, "Unable to open data columns!\nError message: " + e.getMessage(), "Unable to open data columns!", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return;
        }

        try {
            // Get the range of dates the database has and set start and end dates
            LocalDateTime startingDate = databaseHelper.getStartingDate();
            LocalDateTime endingDate = databaseHelper.getEndingDate();

            // Set date time pickers with those dates
            startDateTimePicker.setDateTimeStrict(startingDate);
            endDateTimePicker.setDateTimeStrict(endingDate);
        } catch (SQLException e){
            JOptionPane.showMessageDialog(panelMain, "Unable to set starting and ending dates!\nError message: " + e.getMessage(), "Unable to set starting and ending dates!", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return;
        }
        parameterList.setListData(columns.toArray());

        // Disable Open menu item, enable Close menu item
        openItem.setEnabled(false);
        closeItem.setEnabled(true);
    }

    /**
     * Closes the database connection and resets all fields
     */
    private void closeDatabase() {
        databaseHelper.closeConnection();

        // Clear date time pickers
        startDateTimePicker.clear();
        endDateTimePicker.clear();

        // Disable all controls
        startDateTimePicker.setEnabled(false);
        endDateTimePicker.setEnabled(false);
        applyDateTimeButton.setEnabled(false);
        minimumField.setText("");
        maximumField.setText("");
        minimumField.setEnabled(false);
        maximumField.setEnabled(false);
        applyMaxMinButton.setEnabled(false);

        // Delete all table entries
        Object[] empty = {};
        parameterList.setListData(empty);

        // Purge all tab panes
        tabs.clear();
        tabbedPane1.removeAll();
        // Add empty tab
        addTab(null, null);

        // Re-enable Open item, disable Close item
        openItem.setEnabled(true);
        closeItem.setEnabled(false);
    }

    //endregion

    //region Helper functions

    /**
     * Callback function that is called when user has clicked on a new value in a list
     *
     * @param e The event called when the list item is selected
     */
    private void listValueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting() && parameterList.getSelectedValue() != null) {
            if (tabbedPane1.getTabCount() == 1 && tabbedPane1.getTitleAt(0).equals("No data")) {
                // User has never selected a data param before, delete the current tab and make a new one with data
                removeTab(0);
            }

            // Load metadata of that parameter
            ParameterData data;
            try {
                data = databaseHelper.getParameterData(parameterList.getSelectedValue().toString(), startDateTimePicker.getDateTimeStrict(), endDateTimePicker.getDateTimeStrict());
            } catch (SQLException exception) {
                JOptionPane.showMessageDialog(panelMain, "Error code: " + exception.getErrorCode(), "Error reading from database file!", JOptionPane.ERROR_MESSAGE);
                exception.printStackTrace();
                return;
            }

            // Allow the minimum and maximum fields to be editable
            minimumField.setEnabled(true);
            maximumField.setEnabled(true);
            applyMaxMinButton.setEnabled(true);

            // After loading metadata successfully, add the tab to the JTabbedPane
            addTab(parameterList.getSelectedValue().toString(), data);
        }
    }


    /**
     * Callback to apply the date range from the date pickers
     *
     * @param actionEvent Required for the callback
     */
    private void applyDate(ActionEvent actionEvent) {
        LocalDateTime start = startDateTimePicker.getDateTimeStrict();
        LocalDateTime end = endDateTimePicker.getDateTimeStrict();

        ParameterData data;
        try {
            if (parameterList.getSelectedValue() == null)
                return;
            data = databaseHelper.getParameterData(parameterList.getSelectedValue().toString(), start, end);
        } catch (SQLException exception) {
            JOptionPane.showMessageDialog(panelMain, "Error code: " + exception.getErrorCode(), "Error reading from database file!", JOptionPane.ERROR_MESSAGE);
            exception.printStackTrace();
            return;
        }

        //region Required section to force a refresh of the data in that panel
        int selectedPane = tabbedPane1.getSelectedIndex();
        tabs.get(selectedPane).data = data;
        // Force a refresh
        String[] columns = {"datetime", "data"};
        DefaultTableModel model = new DefaultTableModel(data.dataEntries, columns);
        tabs.get(selectedPane).table.setModel(model);
        tabs.get(selectedPane).table.validate();
        tabs.get(selectedPane).table.repaint();
        //endregion
    }

    /**
     * Callback that applies the maximum and minimum thresholds
     *
     * @param actionEvent Required for the callback
     */
    private void applyMaxMin(ActionEvent actionEvent) {
        int selectedPane = tabbedPane1.getSelectedIndex();

        try {
            tabs.get(selectedPane).data.max = Double.parseDouble(maximumField.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(panelMain, "Invalid maximum threshold!", null, JOptionPane.ERROR_MESSAGE);
            tabs.get(selectedPane).data.max = null;
        }

        try {
            tabs.get(selectedPane).data.min = Double.parseDouble(minimumField.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(panelMain, "Invalid minimum threshold!", null, JOptionPane.ERROR_MESSAGE);
            tabs.get(selectedPane).data.min = null;
        }

        tabs.get(selectedPane).table.validate();
        tabs.get(selectedPane).table.repaint();
    }

    /**
     * Sets the colour of the individual cells. Meant to be embedded in a getTableCellRendererComponent
     * callback.
     *
     * @param value Value of the data in the cell being rendered
     * @param c The default table cell renderer
     * @param data Data that the cell holds
     */
    private void setColor(Object value, Component c, ParameterData data) {
        if (value instanceof Double) {
            Double valueDouble = (Double)value;
            boolean minFlag = false; // true: data is already coloured with minimum, false: data is not coloured
            if (data.min != null) {
                if (valueDouble < data.min) {
                    c.setForeground(Color.WHITE);
                    c.setBackground(Color.BLUE);
                    minFlag = true;
                } else {
                    c.setForeground(Color.BLACK);
                    c.setBackground(Color.WHITE);
                }
            }
            if (data.max != null) {
                if (valueDouble > data.max) {
                    c.setForeground(Color.WHITE);
                    c.setBackground(Color.RED);
                } else if (!minFlag) {
                    // if data is not minimum
                    c.setForeground(Color.BLACK);
                    c.setBackground(Color.WHITE);
                }
            }
        } else {
            c.setForeground(Color.BLACK);
            c.setBackground(Color.WHITE);
        }
    }

    //endregion

    //region Tab management

    /**
     * Callback function for when the tab has changed, either due to user interaction
     * of the tab pane or due to adding or deleting tab panes, which causes the previous
     * tab to be automatically selected
     *
     * @param e The change event
     */
    private void tabChanged(ChangeEvent e) {
        JTabbedPane tabSource = (JTabbedPane) e.getSource();

        // Load metadata into side bar
        if (tabSource.getSelectedIndex() >= 0) { // this is for when there are no tabs displayed
            ParameterData data = tabs.get(tabSource.getSelectedIndex()).data;
            if (data != null) {
                dataTypeLabel.setText("Data Type: " + data.type);
                if (data.unit == null) {
                    unitLabel.setText("Unit: N/A");
                } else {
                    unitLabel.setText("Unit: "+data.unit);
                }

                maximumField.setText(formatDouble(data.max));
                minimumField.setText(formatDouble(data.min));

                String[] columns = {"datetime", "data"};
                DefaultTableModel model = new DefaultTableModel(data.dataEntries, columns);
                tabs.get(tabs.size() - 1).table.setModel(model);

                // Add colour highlighting
                tabs.get(tabs.size() - 1).table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        Component c = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
                        setColor(value, c, data);
                        return c;
                    }
                });
            } else {
                dataTypeLabel.setText("Data Type: N/A");
                unitLabel.setText("Unit: N/A");
            }
        }

    }

    /**
     * Adds a new tab into the tab pane
     *
     * @param title Title of the tab, typically the parameter name is used as the title
     * @param data Data of what the tab should contain
     */
    private void addTab(String title, ParameterData data) {
        tabs.add(new ConsolePane());
        tabs.get(tabs.size() - 1).data = data;
        if (title == null)
            tabbedPane1.addTab("No data", tabs.get(tabs.size() - 1).consolePanel);
        else
            tabbedPane1.addTab(title, tabs.get(tabs.size() - 1).consolePanel);

        // Setup tab here
        if (data == null) {
            tabs.get(tabs.size() - 1).placeholderLabel.setText("No data");
            tabs.get(tabs.size() - 1).scrollPane.setVisible(false);
        } else {
            tabs.get(tabs.size() - 1).placeholderLabel.setVisible(false);
            tabs.get(tabs.size() - 1).scrollPane.setVisible(true);
        }

        // Select tab
        tabbedPane1.setSelectedIndex(tabs.size() - 1);
    }

    /**
     * Removes a tab from the tab pane based on its index
     *
     * @param index Index to remove the tab from
     */
    private void removeTab(int index) {
        tabs.remove(index);
        tabbedPane1.remove(index);
    }

    /**
     * Formats a double into a String while stripping unnecessary 0s (such as 12.0 to 12)
     *
     * @param d Double precision floating point number to convert to String
     * @return A string representative of the double precision floating point number.
     */
    private static String formatDouble(Double d)
    {
        if (d == null)
            return "";
        if(d == (long) d.doubleValue())
            return String.format(Locale.US, "%d",(long)d.doubleValue());
        else
            return String.format(Locale.US, "%s",d);
    }

    //endregion
}
