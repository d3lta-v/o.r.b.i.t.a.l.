package sg.edu.ntu.eee.sarc.orbital;

/**
 * A container class that encapsulates all data for a single parameter within a subsystem
 */
class ParameterData {
    String type, unit;
    Double min, max;
    // dataEntries is a two dimensional object array for storing a simple 2xn table (column x rows)
    Object[][] dataEntries;
}
