package sg.edu.ntu.eee.sarc.orbital;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import javax.swing.*;
import java.io.File;
import java.sql.SQLException;
import java.util.*;

/**
 * DataAssistant is the view controller responsible for presenting the data conversion
 * interface to generate the intermediate file
 */
class DataAssistant {
    //region UI elements
    public JPanel dataAssistantPanel;
    private JButton selectFoldersButton;
    private JTextField dataSourceTextField;
    private JProgressBar progressBar;
    private JButton convertButton;
    private JLabel statusLabel;
    private JComboBox subsystemDropdown;
    //endregion

    //region File management variables
    private File lastDir;
    private ArrayList<File> selectedFolders;
    //endregion

    private DatabaseHelper databaseHelper = new DatabaseHelper();

    DataAssistant() {
        selectFoldersButton.addActionListener(event -> addFolders());
        convertButton.addActionListener(event -> convertData());
        progressBar.setMinimum(0);
        progressBar.setString(" ");
        progressBar.setStringPainted(true);
    }

    /**
     * Presents the user an interface to select the folders containing orbital data
     */
    private void addFolders() {
        // Let user choose the folder
        JFileChooser fileChooser = new JFileChooser(lastDir);
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int showOpenDialog = fileChooser.showOpenDialog(null);
        if (showOpenDialog != JFileChooser.APPROVE_OPTION) {
            // User did not choose anything
            return;
        }
        selectedFolders = new ArrayList<>(Arrays.asList(fileChooser.getSelectedFiles()));
        lastDir = new File(selectedFolders.get(selectedFolders.size() - 1).getParent());

        statusLabel.setText("Status: verifying folder validity...");
        StringBuilder errors = new StringBuilder();

        // First layer sanity check: are these files directories?
        for (File folder : selectedFolders) {
            if (!folder.isDirectory()) {
                errors.append("Not a directory: ");
                errors.append(folder.getName());
                errors.append("\n");
            }
        }
        if (errors.length() > 0) {
            // Break out and display errors
            displayErrors(errors.toString());
            return;
        }

        // Second layer check: does these folders contain "packet_csv"?
        for (File folder : selectedFolders) {
            File packetCsvFolder = new File(folder.getAbsolutePath(), "packet_csv");
            if (!packetCsvFolder.exists() || !packetCsvFolder.isDirectory()) {
                errors.append("Invalid orbital data folder: ");
                errors.append(folder.getName());
                errors.append("\n");
            }
        }
        if (errors.length() > 0) {
            displayErrors(errors.toString());
            return;
        }

        // Third layer check: do these folders combined contain at least 1 orbital data record folder?
        int orbitalFoldersCount = 0;
        for (File folder : selectedFolders) {
            File packetCsvFolder = new File(folder.getAbsolutePath(), "packet_csv");
            File[] records = packetCsvFolder.listFiles(File::isDirectory); // filters all by directory

            if (records != null) {
                orbitalFoldersCount += records.length;
            }
        }
        if (orbitalFoldersCount < 1) {
            // No records
            errors.append("No orbital data records found in the folder(s) selected\n");
        }
        if (errors.length() > 0) {
            displayErrors(errors.toString());
            return;
        }

        // Display the folder name in the text field
        statusLabel.setText("Status: nothing to do");
        StringBuilder folderNames = new StringBuilder();
        for (File file : selectedFolders) {
            if (folderNames.length() != 0) {
                folderNames.append(", ");
            }
            folderNames.append("\"");
            folderNames.append(file.getAbsolutePath());
            folderNames.append("\"");
        }
        dataSourceTextField.setText(folderNames.toString());
        convertButton.setEnabled(true);
    }

    /**
     * Converts data into a centralised intermediate format.
     *
     * WARNING This method does not perform any error checking, so please perform error checking with `addFolders()`!
     */
    private void convertData() {
        // Exact flow of data conversion documented here:
        // 1. Retrieve all CSV files
        // 2. Create database file according to user specifications
        // 3. Read all metadata from one of the CSV files
        // 4. Construct `data` table columns based on `metadata` table rows
        // 5. Read data from all CSVs and populate `data` table with relevant data mapped to columns

        // 1. Retrieves all CSV file directories
        statusLabel.setText("Status: loading CSVs");
        ArrayList<File> orbitalCSVs = new ArrayList<>();
        getAllCSVs(orbitalCSVs);

        // 2. Create database file by asking for where to save
        //region File selector
        JFileChooser fileChooser = new JFileChooser();
        String subsystem = Objects.requireNonNull(subsystemDropdown.getSelectedItem()).toString();
        fileChooser.setSelectedFile(new File("database_" + subsystem + ".db"));
        fileChooser.setDialogTitle("Save database file as...");
        int showSaveDialog = fileChooser.showSaveDialog(dataAssistantPanel);
        if (showSaveDialog != JFileChooser.APPROVE_OPTION) {
            // User did not choose anything
            statusLabel.setText("Status: nothing to do");
            return;
        }
        File databaseDirectoryFile = fileChooser.getSelectedFile();
        String databaseDirectory = databaseDirectoryFile.getAbsolutePath();
        try {
            databaseHelper.openDatabase(databaseDirectory);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(dataAssistantPanel, "Error code: " + e.getErrorCode(), "Error opening or creating database file!", JOptionPane.ERROR_MESSAGE);
            statusLabel.setText("Status: nothing to do");
            e.printStackTrace();
            return;
        }
        //endregion

        // 3. Read for metadata and store in database. This metadata includes the data type of all parameters, min, max, etc.
        statusLabel.setText("Status: storing metadata");
        //region Metadata reading
        try {
            databaseHelper.createMetadata(orbitalCSVs.get(0));
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(dataAssistantPanel, "Error code: " + e.getErrorCode(), "Error creating metadata table!", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return;
        }
        //endregion

        // 4. Construct `data` table columns based on `metadata` table rows
        statusLabel.setText("Status: constructing data table");
        //region Constructing `data` table columns
        try {
            databaseHelper.createDataTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //endregion

        // 5. Actually populate rows from CSV to database
        statusLabel.setText("Status: populating database");
        //region Populate database with parameters
        readRows(orbitalCSVs);
        //endregion
    }

    /**
     * Gets the `File` objects for all CSV files in the selected folders and stores them in the ArrayList
     *
     * WARNING: This function modifies its parameter! Its parameter is a pass-by-reference value, NOT pass-by-value!
     * @param orbitalCSVs
     */
    private void getAllCSVs(ArrayList<File> orbitalCSVs) {
        // Get subsystem to convert from drop-down box
        String subsystem = Objects.requireNonNull(subsystemDropdown.getSelectedItem()).toString(); //this should never be null anyway

        // Top level loop: go through the folders selected by the user
        for (File selectedFolder : selectedFolders) {
            // Second level loop, get the orbital data folders (the ones with a 6-digit running serial number starting at 000000)
            File packetCsvFolder = new File(selectedFolder.getAbsolutePath(), "packet_csv");
            File[] records = packetCsvFolder.listFiles(File::isDirectory); // records is an array with all the serial numbered directories

            assert records != null;
            for (File record : records) {
                // NOTE: This uses a simple .contains() filter, which may not be accurate if another type of data file has a filename that also contains the same string
                File[] orbitalDataFiles = record.listFiles((dir, name) -> {
                    if (name.startsWith(".")) {
                        // This function is to avoid the program from selecting macOS generated metadata files
                        return false;
                    }
                    return name.contains(subsystem);
                });
                assert orbitalDataFiles != null;
                orbitalCSVs.addAll(Arrays.asList(orbitalDataFiles));
            }
        }
        // Run a basic lexicographic sort to sort all by date
        orbitalCSVs.sort(Comparator.comparing(File::getName));
    }

    /**
     * Reads all rows of all the CSV files containing orbital data
     *
     * @param csvFiles An `ArrayList` of `File` objects pointing to CSV files which contain orbital data
     */
    private void readRows(ArrayList<File> csvFiles) {
        // Stage II: Read from every CSV file
        statusLabel.setText("Status: transferring data");
        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setLineSeparator("\r\n"); //CRLF line endings, as the files are generated by Windows
        CsvParser parser = new CsvParser(settings);
        int fileCount = csvFiles.size();
        progressBar.setMaximum(fileCount);

        SwingWorker worker = new SwingWorker() {
            /**
             * Performs a long-running task in a background thread to prevent locking up the UI thread
             * @return Nothing, as of now
             */
            @Override
            protected Object doInBackground() {
                int currentFile = 1;
                for (File csvFile : csvFiles) {
                    publish(currentFile);
                    parser.beginParsing(csvFile);
                    String[] row;

                    String dateTimeString = null;
                    Map<String, String> data = new HashMap<>();
                    boolean dateRetrieved = false;

                    while ((row = parser.parseNext()) != null) {
                        // Get timestamp
                        String offsetString = row[CSVColumnType.OFFSET.getValue()];
                        Integer offset = offsetString.equals("OFFSET") ? 0 : Integer.parseInt(offsetString); //ignore the column headers
                        String unit = row[CSVColumnType.UNIT.getValue()];
                        unit = unit == null ? "" : unit;
                        boolean isUTC = unit.equals("UTC");
                        boolean isNearStartingOffset = offset < 20;
                        if (isUTC && isNearStartingOffset) {
                            dateRetrieved = true;
                            dateTimeString = row[CSVColumnType.VALUE.getValue()];
                        }

                        if (dateRetrieved) {
                            // Once you retrieve the date, you can start parsing all the other stuff
                            data.put(row[CSVColumnType.PARAMETER.getValue()], row[CSVColumnType.VALUE.getValue()]);
                        }
                    }

                    if (dateTimeString != null) {
                        // Begin insert operation into database
                        try {
                            databaseHelper.insertData(dateTimeString, data);
                        } catch (SQLException e) {
                            publish("dateStringError");
                            e.printStackTrace();
                        }
                    } else {
                        // Display error to user using the main thread
                        publish("dateStringError");
                    }

                    currentFile++;
                }

                parser.stopParsing();
                return null;
            }

            /**
             * This function can be used to run tasks on the main UI thread whenever `publish()` is called in the
             * background thread
             * @param chunks Data passed by the `publish()` function in `doInBackground()`
             */
            @Override
            protected void process(List chunks) {
                super.process(chunks); // do we really need this?
                if (chunks.get(0) instanceof String) {
                    JOptionPane.showMessageDialog(dataAssistantPanel, "An entry has a null date object!", "Date is null!", JOptionPane.ERROR_MESSAGE);
                } else {
                    int currentFile = (int)chunks.get(0);
                    progressBar.setValue(currentFile);
                    progressBar.setString(String.format("%s out of %s", String.valueOf(currentFile), String.valueOf(fileCount)));
                }
            }

            @Override
            protected void done() {
                super.done();
                statusLabel.setText("Status: complete!");
                // Just to fill up the bar
                progressBar.setMaximum(1);
                progressBar.setValue(1);
                progressBar.setString("Complete");
            }
        };

        worker.execute();
    }

    /**
     * Displays errors in the current window/panel.
     * @param errors Errors to display, pre-formatted
     */
    private void displayErrors(String errors) {
        statusLabel.setText("Status: error");
        selectedFolders.clear(); // Clear the selected folders, because we don't want to process them if an error occurs
        dataSourceTextField.setText("No directories selected");
        convertButton.setEnabled(false);
        JOptionPane.showMessageDialog(dataAssistantPanel, errors, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
