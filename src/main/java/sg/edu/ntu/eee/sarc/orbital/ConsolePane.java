package sg.edu.ntu.eee.sarc.orbital;

import javax.swing.*;

class ConsolePane {
    // UI Elements declarations
    JPanel consolePanel;
    JLabel placeholderLabel;
    JTable table;
    JScrollPane scrollPane;

    // Data storage
    ParameterData data;

    ConsolePane() {
        table.setDefaultEditor(Object.class, null); // Disable editing
    }
}
