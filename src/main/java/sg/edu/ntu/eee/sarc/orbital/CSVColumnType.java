package sg.edu.ntu.eee.sarc.orbital;

public enum CSVColumnType {

    OFFSET(0),
    INDEX(1),
    PARAMETER(2),
    TYPE(3),
    LENGTH(4),
    RAW(5),
    VALUE(6),
    FORMULA(7),
    INTERPRETED(8),
    UNIT(9),
    MIN(10),
    MAX(11),
    STATUS(12);

    private int intValue;

    CSVColumnType(int value) {
        this.intValue = value;
    }

    public int getValue() {
        return intValue;
    }

    public static CSVColumnType fromInt(int i) {
        for (CSVColumnType b : CSVColumnType .values()) {
            if (b.getValue() == i) { return b; }
        }
        return null;
    }
}