# O.R.B.I.T.A.L. Readme File

Orbital Relational Basic Integrated Tasking Analysis and Listing

Pan Ziyue, last build: 13/4/2018

Singapore Polytechnic

HP: 91478066

Personal Email: random.rrr3@gmail.com

## Basic technical details
This project is written with IntelliJ IDEA Ultimate, which is free for all persons with an email address from educational institutions.

The user interface was built using Swing with IntelliJ's GUI Designer, which generates its own UI code from a WYSIWYG interface builder. This is not compatible with other IDEs.

This project is compiled and debugged with JDK 8 and is run in a JRE 9 environment on the original development computer, but targets JRE 8. This is to ensure maximum backwards compatibility with existing systems installed with JRE 8 while enabling the developer to test with JRE 9, which supports high DPI scaling on screens with high pixel density. It is strongly recommended for the target operating system to be running JRE 9 or above.

This project should be fully compatible with all operating systems, but it is only tested on Windows 10 for the time being.

This project uses Maven for dependency management. To configure the compiler target JRE version, please edit pom.xml. 

The project is compiled into a JAR executable file. Please note that IntelliJ IDEA may not automatically include dependencies into the JAR sometimes, so one must edit it by going to File > Project Structure > Artifacts.

## Expectations for the software

This section defines the specifications, or in other words, expectations of ORBITAL.
 
The core idea of ORBITAL is to automate the grueling process of manually adjusting variables in a MATLAB script.
 
 - Easy analysis of orbital data
 	- Through comparison of different timings of a piece of data
 	- Through visualising a single data from one time to another time
 	- Through a table with a graph to show all that data
 	- Easily export graphs plotted with the system
 
 - Fast retrieval of orbital data
 	- Requires an intermediate file format to convert CSVs to whatever we need. This intermediate file is allowed to grow up to a certain extent.
 	- Allow for batch import into the intermediate file.

## Features

### Data Assistant
This feature is required to parse orbital data from CSV files into an intermediate format

This software uses an SQLite DB to store orbital data in an intermediate format and does not require external dependencies. The rational for storing all this data in a single relational database file is to reduce memory footprint while providing significant speed improvements to data storage, querying and indexing compared to a flat file database like CSV or Excel, as databases are built for such operations.

Operating flow goes roughly like this:

1. User selects subsystem to parse data
2. User selects folders to load orbital data from a folder
3. Program saves output of intermediate file in its own working directory by default but also allows the user to choose where they want to save it

Orbital data parser allows you to parse data from the following subsystems in the VELOX-2 satellite and turn them into intermediate formats: 

* For BGAN: OBDH_BGANFH1 (not found in data given to me, not going to implement)
* For ADCS: ADCS_HK, ADCS_EKF
* For PWRS: PWRS_HK, PCM2_HK
* For fault tolerant payload: OBDH_EXT, INTB_PAYL

Each intermediate file only contains data for ONE subsystem.

### The Console
The console is a monolithic display for all orbital data related operations, including parameter selection, thresholds, plotting and so on.

A typical workflow should look something like this:

1. User has previously converted their data into an intermediate format
2. User executes File > Open to load the intermediate file
3. Left table pane displays all the parameters from that subsystem
4. Top right graph displays a time plot upon selection of a parameter
5. Bottom right table displays data for that parameter
6. User can now perform data analytics:
    * Change the date/time range to narrow down the graph
    * Adjust thresholds of that parameter to highlight non-nominal data points
7. User can enter plotting mode to plot additional data or plot a specialised graph (left panel disabled during plotting mode, right side table will display multiple tabs?)
    * User can plot non-time axis graphs, such as a I-V graph of a solar panel with a scatter plot
    * User can plot side-by-side graphs of two or more variables (TBC)
8. User can now export through:
    * File > Export Graph to export the graph as a PNG
    * File > Export Excel to export that particular range of data (TBC)
9. If the user wants new orbital data, they will have to re-run the Data Assistant with both the old and the new data to generate a new intermediate file for that subsystem. I will consider automating this in the future.

### Data viewer (scrapped, left here for reference only)
This feature loads the intermediate file and allows the user to view a specific portion of the data in a simple table and allows for simple data manipulation to be performed.

When loading the data, the software will prompt for the subsystem you want to load, as the table can only contain data for one subsystem.

Expected features for the data viewer:
* Filter by
    * Date range (date is special because it requires specialised parsing)
    * Any parameter range (update to colour coding thresholds, such as for current, voltage, temperature)
* Sort by
    * Date ascending, descending
    * Any parameter of your choice, will be numerically sorted
* Export to Excel: Export the current data being viewed to Excel for further processing

### Plotter (scrapped, left here for reference only)
To plot a graph, the user has to use the Plotter.

Expected features for the plotter:

* Plot multi-variable time axis graphs. Examples:
    * Temperature fluctuation in the time of 1 orbit
    * Battery voltage fluctuation in the time of 1 orbit
* Plot two-variable scatter graphs. Examples:
    * IV curve of the solar panels
* Export graph as an image

### Feedback
* Keep it as simple to understand as possible. Clear and concise
* Use minimum and maximum values from CSV files and allow user to customise the threshold
* Do not forget the core focus: make life easier
* Consider using the 3 boxes pattern on a single screen instead of a tabbed system
* Table will display multiple data columns when plotting a graph with more than one variable

## Known issues

### JAR does not include certain dependencies
There may be times when the JAR executable does not include dependencies. You can manually reset the JAR artifact creation by doing:

1. Delete "out/artifacts/ORBITAL_jar" folder
2. Delete "src/main/java/resources.META-INF" folder
3. Go to File > Project Structure > Artifacts and delete the exiting JAR artifact
4. Recreate the JAR artifact by choosing + > JAR > From module with dependencies...
5. Set Main class to be "Main"
6. Set Directory for META-INF/MANIFEST.MF to have an extra "/resources" at the back

### Lack of variable checking
The program does not perform a lot of variable checking, such as validating dates. As such, care must be taken to avoid data types that are overly complex as presented in the original CSV files.

### Lack of ability to remove tabs
There is currently no capability to remove a tab using the user interface. The user must use the File > Close feature to remove all tabs.

## Planned features

### Plotter
The plotter user interface was never completed in the time span of the original internship. The planned plotter interface will occupy the upper half of the ConsolePane. Please refer to the Plotter section above.

### Multiple table data
List multiple parameters in the same table and same tab.

### Excel exporting capability 
Export a limited view Excel spreadsheet.

### Export graph
Export a PNG image of the graph plotted using the Plotter.

### Deep integration into MCC
Deep integration into the Mission Control Centre software. Current implementation would just be a button that launches the JAR executable.
 
