# Orbital Data Formats

This document aims to clear up confusion regarding the formats in which the orbital data is presented.

## Folder nesting hierarchy

`YYYY-MM-DD'T'HH'h'mm'm'ss.mss+ZZZZ/packet_csv/nnnnnn/<files>`

Where `nnnnnn` is a 6 digit serial number

## Subsystem data types
* ADCS_HK: Attitude determination and control, housekeeping
* ADCS_EKF: Attitude determination and control, Extended Kalman Filter
* PWRS_HK: Power subsystem, housekeeping
* PCM2_HK: ??
* OBDH_EXT: On-board data handling, EXT?
* INTB_PAYL: ??, Payload

## File naming scheme

### Sample
`2017-09-04T02h55m31.249+0000_00000040_pd_XDATA_OBDH_EXT_READ`

### Formatted
First segment: `YYYY-MM-DD'T'HH'h'mm'm'ss.mss+ZZZZ`

Second Segment: `nnnnnnnn` (8 digits) running serial number

Third Segment: `pd_XDATA_<Subsystem Data Type>_READ`

Acceptable subsystem data types:
`OBDH_BGANFH1, ADCS_HK, ADCS_EKF, PWRS_HK, PCM2_HK, OBDH_EXT, INTB_PAYL`

## Data writing strategy

1. User selects a single folder or several folders with date stamps as the name
2. Program iterates through all of the folders
3. Program iterates through all 6-digit serial numbers for each folder
4. Program reads every CSV file with the `<Subsystem Data Type>` and stores that into intermediate file
5. Program performs sorting of intermediate file by date and time
6. User can now load that intermediate file for analysis

## Lifecycle of the intermediate file
1. File is created by the user through the Data Assistant
2. The data in the intermediate file is never modified from now on
3. ORBITAL reads the intermediate file upon it being loaded by the user
4. The intermediate file is discarded if one needs to analyse new data

## Intermediate file schema
The schema of the file describes the table structure. In other words, think of it as an Excel sheet. Each table in the schema is a sheet on its own, and every column in a table must be accurately described by the schema

Table Name: `data`
Purpose: Stores all of the actual orbital data. Column names are dynamic due to the nature of the data
Columns: date_created (text through https://www.sqlite.org/lang_datefunc.html), column 1, column 2, column 3, ... , column n

Table Name: `metadata`
Purpose: Stores all of the metadata related to that particular parameter
Columns: parameter_name (text), type (text), unit (text), minimum (real), maximum (real)